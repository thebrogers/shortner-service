package ru.ogornostaev.shortner.service;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface ShortnessGenerator {
    String shortness();
}
