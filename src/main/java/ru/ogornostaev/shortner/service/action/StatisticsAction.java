package ru.ogornostaev.shortner.service.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.model.rest.request.StatisticsRequest;
import ru.ogornostaev.shortner.model.rest.response.StatisticsResponse;
import ru.ogornostaev.shortner.service.ShortnessService;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class StatisticsAction implements ServiceAction<StatisticsRequest, StatisticsResponse> {

    @Autowired
    private ShortnessService shortnessService;

    @Override
    public StatisticsResponse process(StatisticsRequest request) {
        return new StatisticsResponse(shortnessService.getRedirectsStatistics(request.getAccountId()));
    }

    @Override
    public boolean supports(Class<? extends RestRequest> requestClass) {
        return requestClass.equals(StatisticsRequest.class);
    }
}
