package ru.ogornostaev.shortner.service.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.ogornostaev.shortner.exception.InternalServiceException;
import ru.ogornostaev.shortner.model.dto.Shortness;
import ru.ogornostaev.shortner.model.rest.request.RegisterRequest;
import ru.ogornostaev.shortner.model.rest.response.RegisterResponse;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.service.ShortnessGenerator;
import ru.ogornostaev.shortner.service.ShortnessService;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class RegisterAction implements ServiceAction<RegisterRequest, RegisterResponse> {

    @Autowired
    private ShortnessService shortnessService;

    @Autowired
    private ShortnessGenerator shortnessGenerator;

    @Override
    public RegisterResponse process(RegisterRequest request) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new InternalServiceException("no authentication");
        }

        String accountId = authentication.getName();

        boolean saved;
        Shortness shortness;

        do {
            shortness = new Shortness()
                    .setShortnessCode(shortnessGenerator.shortness())
                    .setAccountId(accountId)
                    .setOriginalUrl(request.getUrl())
                    .setRedirectType(request.getRedirectType());

            saved = shortnessService.saveIsAbsent(shortness);
        } while (!saved);

        String redirectUrl = ServletUriComponentsBuilder.fromCurrentRequest().replacePath(shortness.getShortnessCode()).toUriString();
        return new RegisterResponse().setShortUrl(redirectUrl);
    }

    @Override
    public boolean supports(Class<? extends RestRequest> requestClass) {
        return requestClass.equals(RegisterRequest.class);
    }
}
