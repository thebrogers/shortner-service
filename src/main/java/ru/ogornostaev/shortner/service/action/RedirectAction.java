package ru.ogornostaev.shortner.service.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.model.dto.Shortness;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.model.rest.request.RedirectRequest;
import ru.ogornostaev.shortner.model.rest.response.RedirectResponse;
import ru.ogornostaev.shortner.service.ShortnessService;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class RedirectAction implements ServiceAction<RedirectRequest, RedirectResponse> {

    @Autowired
    private ShortnessService shortnessService;

    @Override
    public RedirectResponse process(RedirectRequest request) {
        Shortness shortness = shortnessService.incrementAndGet(request.getShortnessCode());

        RedirectResponse response = null;
        if (shortness != null) {
            response = new RedirectResponse()
                    .setRedirectType(shortness.getRedirectType())
                    .setRedirectUrl(shortness.getOriginalUrl());
        }

        return response;
    }

    @Override
    public boolean supports(Class<? extends RestRequest> requestClass) {
        return requestClass.equals(RedirectRequest.class);
    }
}
