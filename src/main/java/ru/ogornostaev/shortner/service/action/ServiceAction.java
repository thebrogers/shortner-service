package ru.ogornostaev.shortner.service.action;

import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.model.rest.RestResponse;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface ServiceAction<REQ extends RestRequest, RESP extends RestResponse> {

    RESP process(REQ request);

    boolean supports(Class<? extends RestRequest> requestClass);
}
