package ru.ogornostaev.shortner.service.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.exception.InternalServiceException;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.model.rest.RestResponse;

import java.util.List;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class ActionsFacade {

    @Autowired
    private List<ServiceAction> actions;

    public RestResponse process(RestRequest restRequest) {
        for (ServiceAction action: actions) {
            if (action.supports(restRequest.getClass())) {
                return action.process(restRequest);
            }
        }

        throw new InternalServiceException("not supported request: " + restRequest);
    }
}
