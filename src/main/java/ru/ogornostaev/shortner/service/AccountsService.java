package ru.ogornostaev.shortner.service;

import ru.ogornostaev.shortner.model.dto.Account;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface AccountsService {
    boolean saveIfAbsent(Account account);
}
