package ru.ogornostaev.shortner.service.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import ru.ogornostaev.shortner.service.PasswordGenerator;

/**
 * Created by ogornostaev 26.02.2017
 */
@Service
public class RandomStringPasswordGenerator implements PasswordGenerator {

    private final static int DEFAULT_PASS_LENGTH = 8;

    private int passwordLength = DEFAULT_PASS_LENGTH;

    public RandomStringPasswordGenerator setPasswordLength(int passwordLength) {
        this.passwordLength = passwordLength;
        return this;
    }

    @Override
    public String password() {
        return RandomStringUtils.random(passwordLength, 0, 0, true, true);
    }
}
