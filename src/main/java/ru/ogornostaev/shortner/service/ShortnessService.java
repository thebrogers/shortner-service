package ru.ogornostaev.shortner.service;

import ru.ogornostaev.shortner.model.dto.Shortness;

import java.util.Map;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface ShortnessService {
    boolean saveIsAbsent(Shortness shortness);

    Map<String, Integer> getRedirectsStatistics(String accountId);

    Shortness incrementAndGet(String shortnessCode);
}
