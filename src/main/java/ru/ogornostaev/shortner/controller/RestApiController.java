package ru.ogornostaev.shortner.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ogornostaev.shortner.model.rest.RestRequest;
import ru.ogornostaev.shortner.model.rest.RestResponse;
import ru.ogornostaev.shortner.model.rest.request.AccountRequest;
import ru.ogornostaev.shortner.model.rest.request.RegisterRequest;
import ru.ogornostaev.shortner.model.rest.request.StatisticsRequest;
import ru.ogornostaev.shortner.service.action.ActionsFacade;

/**
 * Created by ogornostaev 26.02.2017
 */
@RestController
public class RestApiController {

    private final Logger log = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    private ActionsFacade actionsFacade;

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public RestResponse account(@RequestBody AccountRequest request) {
        return process(request);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public RestResponse register(
            @RequestBody RegisterRequest request) {
        return process(request);
    }

    @RequestMapping(value = "/statistics/{accountId}", method = RequestMethod.GET)
    public RestResponse register(
            @PathVariable("accountId") String accountId) {
        return process(new StatisticsRequest().setAccountId(accountId));
    }

    private RestResponse process(RestRequest request) {
        log.info("processing request: {}", request);

        RestResponse response = actionsFacade.process(request);

        log.info("request: {} processed. and return: {}", request, response);
        return response;
    }
}
