package ru.ogornostaev.shortner.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ogornostaev 26.02.2017
 */
@Controller
public class RestExceptionController {

    private final Logger log = LoggerFactory.getLogger(RestExceptionController.class);

    @ExceptionHandler(Throwable.class)
    public @ResponseBody
    ResponseEntity<Object> handleThrowable(Throwable e) {
        log.error("service error", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
