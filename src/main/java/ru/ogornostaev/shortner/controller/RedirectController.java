package ru.ogornostaev.shortner.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.ogornostaev.shortner.exception.InternalServiceException;
import ru.ogornostaev.shortner.model.rest.request.RedirectRequest;
import ru.ogornostaev.shortner.model.rest.response.RedirectResponse;
import ru.ogornostaev.shortner.service.action.ActionsFacade;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ogornostaev 26.02.2017
 */
@Controller
public class RedirectController {

    private final Logger log = LoggerFactory.getLogger(RedirectController.class);

    @Autowired
    private ActionsFacade actionsFacade;

    @RequestMapping(value = "/{shortnessCode}", method = RequestMethod.GET)
    public ResponseEntity<Void> redirect(@PathVariable("shortnessCode") String shortnessCode) {
        log.info("process redirect request for shortness {}", shortnessCode);

        RedirectResponse response = (RedirectResponse) actionsFacade.process(
                new RedirectRequest().setShortnessCode(shortnessCode)
        );

        ResponseEntity<Void> responseEntity;

        if (response == null) {
            log.info("not found mapping for {}", shortnessCode);
            responseEntity = ResponseEntity.notFound().build();
        } else {
            try {
                responseEntity = ResponseEntity
                        .status(response.getRedirectType())
                        .location(new URI(response.getRedirectUrl()))
                        .build();
                log.info("redirecting to {} with status {}", response.getRedirectUrl(), response.getRedirectType());
            } catch (URISyntaxException e) {
                throw new InternalServiceException("invalid redirect URI format", e);
            }
        }

        return responseEntity;
    }
}
