package ru.ogornostaev.shortner.utils;

import ru.ogornostaev.shortner.exception.InternalServiceException;

import java.security.MessageDigest;
import java.util.Base64;

/**
 * Created by ogornostaev 26.02.2017
 */
public class PasswordHashGenerator {
    private PasswordHashGenerator() {
    }

    public static String getPasswordHash(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());
            byte[] aMessageDigest = md.digest();

            return Base64.getEncoder().encodeToString(aMessageDigest);
        } catch (Exception e) {
            throw new InternalServiceException("password hash preparing failed", e);
        }
    }
}
