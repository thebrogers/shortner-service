package ru.ogornostaev.shortner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ogornostaev.shortner.model.dto.Shortness;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface ShortnessRepository extends CrudRepository<Shortness, String> {
}
