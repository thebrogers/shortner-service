package ru.ogornostaev.shortner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ogornostaev.shortner.model.dto.Account;

/**
 * Created by ogornostaev 26.02.2017
 */
public interface AccountsRepository extends CrudRepository<Account, String> {
}
