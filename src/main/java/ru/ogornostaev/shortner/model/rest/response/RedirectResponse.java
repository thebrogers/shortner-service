package ru.ogornostaev.shortner.model.rest.response;

import ru.ogornostaev.shortner.model.rest.RestResponse;

/**
 * Created by ogornostaev 26.02.2017
 */
public class RedirectResponse implements RestResponse {

    private int redirectType;
    private String redirectUrl;

    public int getRedirectType() {
        return redirectType;
    }

    public RedirectResponse setRedirectType(int redirectType) {
        this.redirectType = redirectType;
        return this;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public RedirectResponse setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
        return this;
    }

    @Override
    public String toString() {
        return "RedirectResponse{" +
                "redirectType=" + redirectType +
                ", redirectUrl='" + redirectUrl + '\'' +
                '}';
    }
}
