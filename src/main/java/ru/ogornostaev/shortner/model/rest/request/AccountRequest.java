package ru.ogornostaev.shortner.model.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ogornostaev.shortner.model.rest.RestRequest;

/**
 * Created by ogornostaev 26.02.2017
 */
public class AccountRequest implements RestRequest {
    @JsonProperty(value = "AccountId", required = true)
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public AccountRequest setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    @Override
    public String toString() {
        return "AccountRequest{" +
                "accountId='" + accountId + '\'' +
                '}';
    }
}
