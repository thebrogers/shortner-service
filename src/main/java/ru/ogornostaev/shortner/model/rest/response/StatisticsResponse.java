package ru.ogornostaev.shortner.model.rest.response;

import ru.ogornostaev.shortner.model.rest.RestResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ogornostaev 26.02.2017
 */
public class StatisticsResponse extends HashMap<String, Integer> implements RestResponse {

    public StatisticsResponse(Map<? extends String, ? extends Integer> m) {
        super(m);
    }
}
