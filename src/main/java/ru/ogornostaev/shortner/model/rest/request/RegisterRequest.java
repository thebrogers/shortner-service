package ru.ogornostaev.shortner.model.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ogornostaev.shortner.model.rest.RestRequest;

/**
 * Created by ogornostaev 26.02.2017
 */
public class RegisterRequest implements RestRequest {
    @JsonProperty(required = true)
    private String url;

    @JsonProperty(required =  true)
    private int redirectType;

    public String getUrl() {
        return url;
    }

    public RegisterRequest setUrl(String url) {
        this.url = url;
        return this;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public RegisterRequest setRedirectType(int redirectType) {
        this.redirectType = redirectType;
        return this;
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "url='" + url + '\'' +
                ", redirectType=" + redirectType +
                '}';
    }
}
