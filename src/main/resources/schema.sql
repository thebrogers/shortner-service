CREATE TABLE IF NOT EXISTS accounts (
  account_id VARCHAR(50) NOT NULL,
  password_hash VARCHAR(100) NOT NULL,
  PRIMARY KEY (account_id)
);

CREATE TABLE IF NOT EXISTS shortness (
  shortness_code VARCHAR(20) NOT NULL,
  account_id VARCHAR(50) NOT NULL,
  original_url VARCHAR(1024) NOT NULL,
  redirect_type INT NOT NULL,
  redirects_count INT NOT NULL DEFAULT 0,
  PRIMARY KEY (shortness_code)
);

CREATE INDEX IF NOT EXISTS idx_account_id ON shortness (account_id);