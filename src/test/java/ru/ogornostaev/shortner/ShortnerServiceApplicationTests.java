package ru.ogornostaev.shortner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import ru.ogornostaev.shortner.model.dto.Shortness;
import ru.ogornostaev.shortner.repository.AccountsRepository;
import ru.ogornostaev.shortner.model.dto.Account;
import ru.ogornostaev.shortner.model.rest.request.AccountRequest;
import ru.ogornostaev.shortner.model.rest.request.RegisterRequest;
import ru.ogornostaev.shortner.repository.ShortnessRepository;
import ru.ogornostaev.shortner.utils.PasswordHashGenerator;

import javax.servlet.Filter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
@WebAppConfiguration
public class ShortnerServiceApplicationTests {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private AccountsRepository accountsRepository;

    @Autowired
    private ShortnessRepository shortnessRepository;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext)
                .addFilters(springSecurityFilterChain).build();
        this.accountsRepository.deleteAll();
        this.shortnessRepository.deleteAll();
    }


    @Test
    public void testSaveAccount() throws Exception {
        mockMvc.perform(
                post("/account")
                        .content(this.json(new AccountRequest().setAccountId("test")))
                        .contentType(contentType)
        ).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.description", is("Your account is opened")))
                .andExpect(jsonPath("$.password", notNullValue()));

    }

    @Test
    public void testSaveDuplicateAccount() throws Exception {
        accountsRepository.save(new Account().setAccountId("duplicate")
                .setPasswordHash(PasswordHashGenerator.getPasswordHash("123")));

        mockMvc.perform(
                post("/account")
                        .content(this.json(new AccountRequest().setAccountId("duplicate")))
                        .contentType(contentType)
        ).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.success", is(false)))
                .andExpect(jsonPath("$.description", is("Account with that ID already exists")))
                .andExpect(jsonPath("$.password").doesNotExist());
    }

    @Test
    public void testRegisterShortness() throws Exception {
        accountsRepository.save(new Account().setAccountId("test")
                .setPasswordHash(PasswordHashGenerator.getPasswordHash("123")));

        mockMvc.perform(
                post("/register").with(httpBasic("test", "123"))
                        .content(this.json(new RegisterRequest().setRedirectType(301).setUrl("http://test.com")))
                        .contentType(contentType)
        ).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shortUrl", startsWith("http://localhost/")));
    }

    @Test
    public void testShortnessStatistics() throws Exception {
        accountsRepository.save(new Account().setAccountId("test")
                .setPasswordHash(PasswordHashGenerator.getPasswordHash("123")));

        Shortness shortness = new Shortness()
                .setShortnessCode("1")
                .setRedirectType(301)
                .setOriginalUrl("http://1.com")
                .setAccountId("test")
                .setRedirectsCount(5);

        shortnessRepository.save(shortness);

        shortnessRepository.save(shortness
                .setShortnessCode("2")
                .setOriginalUrl("http://2.com")
                .setRedirectsCount(10)
        );

        shortnessRepository.save(shortness
                .setShortnessCode("3")
                .setOriginalUrl("http://3.com")
                .setRedirectsCount(1)
                .setAccountId("test2")
        );

        mockMvc.perform(
                get("/statistics/test").with(httpBasic("test", "123"))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.['http://1.com']", is(5)))
                .andExpect(jsonPath("$.['http://2.com']", is(10)))
                .andExpect(jsonPath("$.['http://3.com']").doesNotExist());
    }

    @Test
    public void testShortnessRedirect() throws Exception {
        accountsRepository.save(new Account().setAccountId("test")
                .setPasswordHash(PasswordHashGenerator.getPasswordHash("123")));

        Shortness shortness = new Shortness()
                .setShortnessCode("short1")
                .setRedirectType(301)
                .setOriginalUrl("http://1.com")
                .setAccountId("test")
                .setRedirectsCount(10);

        shortnessRepository.save(shortness);

        shortnessRepository.save(shortness
                .setShortnessCode("short2")
                .setRedirectType(302)
                .setOriginalUrl("http://2.com")
                .setRedirectsCount(5)
        );

        mockMvc.perform(
                get("/short1")
        ).andExpect(status().is(301))
                .andExpect(redirectedUrl("http://1.com"));

        mockMvc.perform(
                get("/short2")
        ).andExpect(status().is(302))
                .andExpect(redirectedUrl("http://2.com"));

        assertEquals(11,  shortnessRepository.findOne("short1").getRedirectsCount());
        assertEquals(6,  shortnessRepository.findOne("short2").getRedirectsCount());
    }


    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
